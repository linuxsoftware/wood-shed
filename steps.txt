WATCH
https://www.youtube.com/watch?v=4K8iwBVFHME
https://www.youtube.com/watch?v=ULgbsxyZVCQ

DONE
Dig holes
Attach straps to posts J,K,B,C
Notch posts A,D,I,L
Place and brace posts in positions
  * check vertical and in line
Screw beam midSout on
Cut recesses in midSout
Concrete posts in
Attach beams botNin to I,L and botSin to A,D with screws
  * should be resting on J,K and B,C
  * check horizontal
Bolt straps to connect posts J1,K1 to J,K
  * check vertical
  * brace
Test fit and trim beams botNout, botSout
Cut and fit braces B1W,B1E to post B1 and C1W,C1E to post C1
Bolt straps to connect posts B1,C1 to B,C
  * check vertical
  * brace
Place beam botCen on posts F,Ƕ,G
  * check horizontal
Bolt and screw plates botW and botE on
  * trim botCen as required
Attach botCen to plates botW,botE using joist hangers
Attach botCen to F,Ƕ,G using lumberlocks
Add 6 cross supports from botCen to outer beams
Screw beams botNout and botSout on
Cut recesses in botSout
Bolt bottom beams on (botNout-botNin, botSout-botSin)
  * cup head bolts on South wall to be flush as to be covered by plywood
Check dimensions and assemble louvres
Remove bracing from posts as needed for access
Screw in louvreW and louvreE
  * bottom flush with plates botW or botE
Bolt beam midSout on
  * check horizontal
  * M16 240 bolts go through both I,L and louvre studs
Attach beam topSin to A,B,C,D with screws
  * check horizontal
Pull posts A,D,I,L to vertical
Test fit and trim beam topSout
Bolt and screw plates topW and topE on
  * trim topSout as required
Attach beam topSout to I,J,K,L
  * check horizontal
Cut recesses in topSout
Bolt topSout-topSin
  * cup head bolts on South wall to be flush as to be covered by plywood
Screw braces AW to post A and DE to post D
Insert 4* extra 4x2" spacers between South beams at rafter load points
Attach 8* battens to South wall (toenail / nailplates / straps ?)
Screw beam topNin on
Bolt top beams on topNout-topNin
Trim plywood sheets to 2.1m
Trim Eastmost panel to clear slope of ground 
Screw sheets of plywood onto South wall
  * 5mm gap betweed sheets
  * if cupped, attach bulging outwards
Birds mouth notch rafters
Attach rafters using lumberlocks / cyclone clips

DOING
Attach purloins + trim with screws
Fix corrugated iron on
Add floor boards

